# -*- coding: utf-8 -*-
''' @file                       Task_Data.py
    @brief                      A driver for acquiring data from IMU and touch panel
    @details
    @author                     Tyler McCue
    @date                       January 1, 1970
'''

import utime


class Task_Data:
    ''' @brief        Imports share to save current data
        @details
    '''

    def __init__(self,  req_period):
        ''' @brief
            @details
            @param req_period Period at which data task will run
        '''
        ## Required Period: Determines how frequently the encoder count
        self.freq = req_period
        # Convert encoder task frequency from sec to microsec
        self.freq = int(self.freq*1E6)

        ## Next Time: Time at which task will next run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.freq)



    def update(self, s):
        ''' @brief
            @details            Update data at every time interval
            @param      s       Shared data
            @return             Data from balance system
        '''
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) > 0):
            #print(utime.ticks_us())
            self.next_time = utime.ticks_add(self.next_time, self.freq)

            if s.active:

                coords = s.TS.Scan(self.freq/(1E6))
                angles = s.IMU.get_Euler_angles()
                rot = s.IMU.get_Rotation()
                s.DataX = [coords[0], angles[1], coords[2], rot[1]]
                s.DataY = [coords[1], angles[2], coords[3], rot[2]]
                s.touch = coords[4]
