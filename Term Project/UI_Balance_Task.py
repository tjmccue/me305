'''
@file           UI_Balance_Task.py
@brief          User Interface on Putty Terminal
@details        Uses micro python VCP module to accept user inputs
                one character at a time. These user inputs command action
                by the balance system.
@author         Tyler McCue
@date           Dec 7th, 2021
'''

import pyb
import utime
import time
import array as arr


class UI_Balance_Task():
    '''
    @brief      UI class that allows balance plate to take user inputs
    @details    Uses micro python VCP module to accept user inputs
                for balance system
    '''

    def __init__(self, period):
        '''
        @brief          Constructor for UI driver
        @param period   Period of time at which user task runs, in sec
        '''
        ## Set input period equal to the frequency at which the user task will
        # run
        self.user_freq = period
        # Convert user task period from sec to microsec
        self.user_freq = int(self.user_freq*1E6)

        ## Assign VP communication for specified encoder
        # Allows for reading and writing of data over the USB connecting user
        #   & Nucleo
        self.coms = pyb.USB_VCP()

        ## Initiate input variable associated with user key presses
        self.input = '0'


        ## Next_Time determines the time at which the user task will look for
        #  user inputs
        self.next_time = utime.ticks_add(utime.ticks_us(), self.user_freq)

        self.IMUcalib = False
        self.TScalib = False
        self.first = False

        self.printIntro()


    def printIntro(self):
        ''' @brief   Prints UI Commands
            @details Prints all UI commands and inputs each time called
        '''
        # Welcome instructions printed to Putty terminal on start-up
        print('+-----------------------------------------------------------+')
        print('|                  Welcome to BalanceUI                     |')
        print('|                                                           |')
        print('| Balance Commands:                                         |')
        print('| 1. "i": Calibrate IMU                                     |')
        print('| 2. "t": Calibrate Touch Panel                             |')
        print('| 3. "s": Enable balance control                            |')
        print('| 4. "d": Disable balance control                           |')
        print('| 5. "c": Start Data Collection                             |')
        print('| 6. "q": Stop Data Collection                              |')
        print('| 7. "x": Change Kx Vector                                  |')
        print('| 8. "y": Change Ky Vector                                  |')
        print('|                                                           |')
        print('+-----------------------------------------------------------+')

    def getInput(self, s):
        '''
        @brief              Reads user inputs and writes encoder/motor commands
        @param s            Refers to shares class which shares needed data
        '''
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) > 0):
            self.next_time = utime.ticks_add(self.next_time, self.user_freq)
            self.state = 0
            # Read one character of user key press
            if self.coms.any():
                self.input = self.coms.read(1)

                # Encoder/Motor 1 key presses and associated commands
                if self.input.decode() == 'i':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Please move panel until calibration is complete!")
                    s.IMU.change_mode("NDOF")
                    while s.IMU.get_calibration_status() != b'\xff':
                        time.sleep(.5)
#                        print("\n"*25)
#                        print("Calibration Status at:")
#                        print(round((s.IMU.get_calibration_status()[0]/255)*100, 1), "%")
                    print("Calibration Complete")
                    self.IMUcalib = True
                if self.input.decode() == 't':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    s.TS.calibrate()
                    self.TScalib = True
                if self.input.decode() == 's':
                    if self.IMUcalib and self.TScalib:
                        print('\n'*25)
                        self.printIntro()
                        print('\n'*5)
                        print("Activating Balance Control")
                        s.active = True
                    else:
                        print('\n'*25)
                        self.printIntro()
                        print('\n'*5)
                        print('Please calibrate both the touch panel and IMU!')
                if self.input.decode() == 'd':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Deactivating Balance Control")
                    s.active = False
                if self.input.decode() == 'c':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Data Collection Active")
                    s.collect = True
                    self.first = True
                if self.input.decode() == 'q':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Data Collection Stopped")
                    s.collect = False
                    self.first = False
                if self.input.decode() == 'x':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Setting Kx Vector")
                    print("Previous Kx Vector: ", s.Control.kx)
                    Kx = input("Enter Kx vector in order of (X Theta X_Dot Theta_Dot): ")
                    Kx = Kx.split()
                    s.Control.setKx(Kx)
                if self.input.decode() == 'y':
                    print('\n'*25)
                    self.printIntro()
                    print('\n'*5)
                    print("Setting Ky Vector")
                    print("Previous Ky Vector: ", s.Control.ky)
                    Ky = input("Enter Ky vector in order of (Y Theta Y_Dot Theta_Dot): ")
                    Ky = Ky.split()
                    s.Control.setKy(Ky)
            if s.collect and s.active:
                if self.first:
                    print("PositionX AngleX VeloX VRadX PositionY AngleY VeloY VRadY Time")
                    self.first = False
                data = []
                data += s.DataX
                data += s.DataY
                data.append(utime.ticks_us()/1e6)
                Sdata = ' '.join(str(r) for r in data)
                print(Sdata)
