''' @file                       task_encoder.py
    @brief                      A driver for reading from Quadrature Encoders
    @details
    @author                     Nick De Simone, Charlie Refvem for run code
    @date                       January 1, 1970
'''


import encoder
import utime

S0_INIT = 0
S1_ZERO = 1

class Task_Encoder:
    ''' @brief        Import Encoder class to return position updates to main
        @details
    '''

    def __init__(self, encoder_num, req_period):
        ''' @brief
            @details
            @param encoder_num Encoder defined by main as either the 1 or 2
                                physical encoder
            @param req_period Period at which encoder task will run
        '''
        ## Required Period: Determines how frequently the encoder count
        #  will be updated
        self.encoder_freq = req_period
        # Convert encoder task frequency from sec to microsec
        self.encoder_freq = int(self.encoder_freq*1E6)

        # Class variable describing the appropriate encoder number (1 or 2)
        self.encoder_num = encoder_num

        # Create object of Encoder class which allows driving of associated
        #   encoder number (encoder_num)
        self.encoder_obj = encoder.Encoder(self.encoder_num)




        self.runs = 0
        self.next_time = utime.ticks_add(utime.ticks_us(), self.encoder_freq)
        self.current = [0,0,0]

    def get_update(self, s):
        if self.encoder_num == 1:
            self.update(s.state[0], s, 1)
        else:
            self.update(s.state[1], s, 2)


    def update(self, state, s, encoder):
        ''' @brief
            @details            Update position at specified interval
            @param      state   Refers to state of encoder
            @param      s       Shared data
            @param      encoder Encoder being updated
            @return             The position and velocity of the encoder shaft
        '''
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) > 0):
            #print(utime.ticks_us())
            self.next_time = utime.ticks_add(self.next_time, self.encoder_freq)
            if (state == S0_INIT):
                self.encoder_obj.update()
                if encoder == 1:
                    s.data1 = [self.encoder_obj.get_position(), self.encoder_obj.get_delta(), utime.ticks_us()]
                else:
                    s.data2 = [self.encoder_obj.get_position(), self.encoder_obj.get_delta(), utime.ticks_us()]
            elif (state == S1_ZERO):
                self.encoder_obj.set_position(0)
                s.state = [0,0]
                self.encoder_obj.update()
                if encoder == 1:
                    s.data1 = [self.encoder_obj.get_position(), self.encoder_obj.get_delta(), utime.ticks_us()]
                else:
                    s.data2 = [self.encoder_obj.get_position(), self.encoder_obj.get_delta(), utime.ticks_us()]
