''' @file                       IMU_driver.py
    @brief                      A driver for implementing IMU
    @author                     Tyler McCue
    @date                       January 1, 1970
'''
from pyb import I2C
import struct as st

class IMU_Driver:
    ''' @brief        Import IMU class to read angles and accelerations
        @details      Uses I2C to read values from IMU
    '''

    def __init__(self, id=0x28):
        ''' @brief     Constructor for IMU
            @param id register for IMU
        '''
        self.i2c = I2C(1)
        self.i2c = I2C(1, I2C.MASTER)
        self.buff = bytearray()
        ## ID for IMU register
        self.id = id

        # Mode Value: Decimal Value
        # return decimal value after specifying mode value
        ## Dictionary with mode values for IMU
        self.mode = {"ACCONLY": 1,
        "MAGONLY": 2,
        "GYROONLY": 3,
        "ACCMAG": 4,
        "ACCGYRO": 5,
        "MAGGYRO": 6,
        "AMG": 7,
        "IMU": 8,
        "COMPASS": 9,
        "NDOF_FMC_OFF": 11,
        "NDOF": 12}

    def change_mode(self, mode_value):
        '''
        @brief              Changes mode for IMU
        @param mode_value   Integer representing value of mode in dictionary
        '''
        self.i2c.mem_write(self.mode.get(mode_value), self.id, 0x3D)

    def get_calibration_status(self):
        '''
        @brief         Gets calibration status for IMU
        @return        Returns bytes of each calibration status
        '''
        return self.i2c.mem_read(1,40,35)

    def get_calibration_coeffs(self):
        '''
        @brief         Gets calibration coefficents for IMU
        @return        Returns bytes of each calibration coefficent
        '''
        buff = bytearray(22)
        return st.unpack('bbbbbbbbbbb', self.i2c.mem_read(buff,40,55))

    def write_calibration_coeffs(self, b_buffer):
        '''
        @brief              Writes calibration coefficents for IMU
        @param  b_buffer    Buffer of calibration coefficents
        '''
        self.i2c.mem_write(b_buffer,40,55)

    def get_Euler_angles(self):
        '''
        @brief              Reads Euler angles from IMU
        @return             Euler angles from IMU
        '''
        head = bytearray(self.i2c.mem_read(2,40,0x1A))
        head_value = int.from_bytes(head, "little", False)
        roll = bytearray(self.i2c.mem_read(2,40,0x1C))
        roll_value = int.from_bytes(roll, "little", False)
        pitch = bytearray(self.i2c.mem_read(2,40,0x1E))
        pitch_value = int.from_bytes(pitch, "little", False)
        if head_value > 32767:
            head_value -= 65536
        if roll_value > 32767:
            roll_value -= 65536
        if pitch_value > 32767:
            pitch_value -= 65536
        return head_value/16, pitch_value/16, roll_value/16

    def get_Rotation(self):
        '''
        @brief              Reads rotational velocities from IMU
        @return             Rotational velocities from IMU
        '''
        pitch = bytearray(self.i2c.mem_read(2,40,0x14))
        pitch_value = int.from_bytes(pitch, "little", False)
        roll = bytearray(self.i2c.mem_read(2,40,0x16))
        roll_value = int.from_bytes(roll, "little", False)
        head = bytearray(self.i2c.mem_read(2,40,0x18))
        head_value = int.from_bytes(head, "little", False)
        if head_value > 32767:
            head_value -= 65536
        if roll_value > 32767:
            roll_value -= 65536
        if pitch_value > 32767:
            pitch_value -= 65536
        return head_value/16, pitch_value/16, roll_value/16
