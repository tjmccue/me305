''' @file                       main.py
    @brief                      Main program that runs balance plate
    @author                     Tyler McCue
    @date                       January 1, 1970
'''

import IMU_driver as imu
import DRV8847_Motor as Dr
import Touch_Screen as ts
import shares as share
import UI_Balance_Task as UI
import Task_Data as Data
import Task_Control as Control



driver = Dr.DRV8847()
touch = ts.TouchScreen(160,80)
mes = imu.IMU_Driver()
user = UI.UI_Balance_Task(.025)
Dats = Data.Task_Data(.010)
Con = Control.Task_Control(.010)

## Create Motor objects
# Specify motor number
motor1 = driver.motor(1)
motor2 = driver.motor(2)


s = share.Shares(motor1, motor2, mes, touch, Con)


print("\n\n\n\n\n")
while True:
    user.getInput(s)
    Dats.update(s)
    Con.Control(s)
