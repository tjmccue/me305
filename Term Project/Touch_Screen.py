''' @file                       Touch_Screen.py
    @brief                      Driver for touch screen on plate
    @author                     Tyler McCue
    @date                       January 1, 1970
'''
import pyb
from pyb import *

class TouchScreen():

    def __init__(self, x, y):
        ''' @brief    Contstuctor for touch panel class
            @param    x Width of touch panel
            @param    y Height of touch panel
        '''
        ## If touch panel is calibrated
        self.calibrated = False
        ## Length of panel
        self.xlength = x
        ## Height of panel
        self.ylength = y
        ## Calibration offset
        self.zeroy = 0
        ## Calibration offset
        self.zerox = 0
        ## Calibration slope
        self.slopex = 0
        ## Calibration slope
        self.slopey = 0
        ## Last X Velocity
        self.lastVeloX = 0
        ## Last Y Velocity
        self.lastVeloY = 0
        ## Last X position
        self.lastPosX = 0
        ## Last Y position
        self.lastPosY = 0
        ## Alpah variable for AB Filter
        self.alpha = .85
        ## Beta variable for AB Filter
        self.beta = .005

    def Scan(self, freq):
        ''' @brief    Scans panel for X,Y and velocities
            @param    freq Frequency panel is scanned at
            @return   X position along X
            @return   Y position along Y
            @return   Vx velocity along X
            @return   Vy velocity along Y
        '''
        pinXp = pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.OUT_PP)
        pinXp.value(True)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.OUT_PP)
        pinXm.value(False)
        pinYp =  pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.IN)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.IN)
        adcX = pyb.ADC(pinYm)
        X = adcX.read()*self.slopex + self.zerox - self.xlength/2

        pinYp = pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.OUT_PP)
        pinYp.value(True)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.OUT_PP)
        pinYm.value(False)
        pinXp =  pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.IN)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.IN)
        adcY = pyb.ADC(pinXm)
        Y = adcY.read()*self.slopey + self.zeroy - self.ylength/2

        pinYp = pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.OUT_PP)
        pinYp.value(True)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.OUT_PP)
        pinXm.value(False)
        pinXp =  pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.IN)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.IN)
        adcZ = pyb.ADC(pinXp)
        if adcZ.read() > 41:
            Z = True
        else:
            Z = False

        deltaT = freq

        predX = self.lastPosX + deltaT*self.lastVeloX
        predY = self.lastPosY + deltaT*self.lastVeloY
        errorX = X - predX
        errorY = Y - predY
        X = predX + self.alpha*errorX
        Y = predY + self.alpha*errorY
        veloX = self.lastVeloX + (self.beta/deltaT)*errorX
        veloY = self.lastVeloY + (self.beta/deltaT)*errorY
        self.lastPosX = X
        self.lastPosY = Y
        self.lastVeloX = veloX
        self.lastVeloY = veloY

        if not Z:
            X = 0
            Y = 0
            veloX = 0
            veloY = 0
        return (X, Y, veloX, veloY, Z)

    def calibrate(self):
        ''' @brief    Calibrates touch panel from two points
        '''
        DataX1 = []
        DataX2 = []
        DataY1 = []
        DataY2 = []
        print('Touch One Corner ONLY CORNER')
        while len(DataX1) < 50 and len(DataY1) < 50:
            if self.Zscan():
                DataX1.append(self.Xscan())
                DataY1.append(self.Yscan())

        while self.Zscan():
            pass
        print('Touch Opposite Corner ONLY CORNER')

        while len(DataX2) < 50 and len(DataY2) < 50:
            if self.Zscan():
                DataX2.append(self.Xscan())
                DataY2.append(self.Yscan())
        print('Calibration Complete')

        X1 = sum(DataX1)/len(DataX1)
        X2 = sum(DataX2)/len(DataX2)
        Y1 = sum(DataY1)/len(DataY1)
        Y2 = sum(DataY2)/len(DataY2)

        if X1 > X2:
            self.slopex = self.xlength/(X1-X2)
            self.zerox = -X2*self.slopex
        else:
            self.slopex = self.xlength/(X2-X1)
            self.zerox = -X1*self.slopex

        if Y1 > Y2:
            self.slopey = self.ylength/(Y1-Y2)
            self.zeroy = -Y2*self.slopey
        else:
            self.slopey = self.ylength/(Y2-Y1)
            self.zeroy = -Y1*self.slopey

    def Xscan(self):
        ''' @brief    Scans for X value independently
            @return   X position
        '''
        pinXp = pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.OUT_PP)
        pinXp.value(True)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.OUT_PP)
        pinXm.value(False)

        pinYp =  pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.IN)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.IN)

        adcX = pyb.ADC(pinYm)
        return adcX.read()


    def Yscan(self):
        ''' @brief    Scans for Y value independently
            @return   Y position
        '''
        pinYp = pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.OUT_PP)
        pinYp.value(True)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.OUT_PP)
        pinYm.value(False)

        pinXp =  pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.IN)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.IN)

        adcY = pyb.ADC(pinXm)
        return adcY.read()

    def Zscan(self):
        ''' @brief    Scans if panel is touched
            @return   Boolean if panel is being touched
        '''
        pinYp = pyb.Pin(pyb.Pin.cpu.A6, mode=pyb.Pin.OUT_PP)
        pinYp.value(True)
        pinXm =  pyb.Pin(pyb.Pin.cpu.A1, mode=pyb.Pin.OUT_PP)
        pinXm.value(False)

        pinXp =  pyb.Pin(pyb.Pin.cpu.A7, mode=pyb.Pin.IN)
        pinYm =  pyb.Pin(pyb.Pin.cpu.A0, mode=pyb.Pin.IN)

        adcZ = pyb.ADC(pinXp)
        if adcZ.read() > 41:
            return True
        else:
            return False
