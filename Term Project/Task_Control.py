''' @file                       Task_Control.py
    @brief                      A driver for implementing controls on balance plate
    @author                     Tyler McCue
    @date                       January 1, 1970
'''

import utime
from ulab import numpy as np
import math as m

V_DC = 12   #Volts
R = 2.21    #Ohms
Kt = 13.8   #mNm/A

Duty2Torque = 100*R/(4*(Kt/1000)*V_DC)

Ang2Rad = m.pi/180


class Task_Control:
    ''' @brief        Import control class
    '''

    def __init__(self, req_period):
        ''' @brief    Contstuctor for controller task
            @param    req_period Period at which encoder task will run
        '''
        ## Required Period: Determines how frequently the encoder count
        #  will be updated
        self.freq = req_period
        # Convert encoder task frequency from sec to microsec
        self.freq = int(self.freq*1E6)

        ## K vector for y direction
        self.ky = np.array([[-1.2, -.5, -.0185, .2e-1]])

        ## K vector for x direction
        self.kx = np.array([[0.825, .5, .0185, -.2e-1]])

        ## K vector for x direction with no ball
        self.kx_p = np.array([[0,   -1.2,   0,  0.05]])

        ## K vector for y direction with no ball
        self.ky_p = np.array([[0,   -1.3,   0,  0.05]])

        ## Next time to run task
        self.next_time = utime.ticks_add(utime.ticks_us(), self.freq)

    def Duty(self, T):
        ''' @brief              Torque to Duty Cycle
            @details            Converts torque to duty cycle
            @param      T       Torque value required
        '''
        if T >= 0:
            duty = min(Duty2Torque*T, 100)
        else:
            duty = max(Duty2Torque*T, -100)
        return duty

    def setKx(self, Kx):
        ''' @brief              Changes K vector
            @details            Sets new K vectory for x-axis
            @param      Kx      New K vector
        '''
        self.kx = np.array([[float(Kx[0]), float(Kx[1]), float(Kx[2]), float(Kx[3])]])

    def setKy(self, Ky):
        ''' @brief              Changes K vector
            @details            Sets new K vectory for y-axis
            @param      Ky      New K vector
        '''
        self.ky = np.array([[float(Ky[0]), float(Ky[1]), float(Ky[2]), float(Ky[3])]])

    def Control(self, s):
        ''' @brief              Controls for balance plate
            @details            Updates torque value for balance system and converts to duty cycle
            @param      s       Shared data
        '''
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) > 0):
            self.next_time = utime.ticks_add(self.next_time, self.freq)

            if s.active:
                UX = (np.array([s.DataX[0]/1000, s.DataX[1]*Ang2Rad, s.DataX[2]/1000, s.DataX[3]*Ang2Rad])).reshape((4,1))
                UY = (np.array([s.DataY[0]/1000, s.DataY[1]*Ang2Rad, s.DataY[2]/1000, s.DataY[3]*Ang2Rad])).reshape((4,1))

                if s.touch:
                    Tx = np.dot(self.kx, UX)[0,0]
                    Ty = np.dot(self.ky, UY)[0,0]

                else:
                    Tx = -np.dot(self.kx_p, UX)[0,0]
                    Ty = np.dot(self.ky_p, UY)[0,0]

                dutyX = self.Duty(Tx)
                dutyY = self.Duty(Ty)

                s.m1.set_duty(dutyX)
                s.m2.set_duty(dutyY)
            else:
                s.m1.set_duty(0)
                s.m2.set_duty(0)
