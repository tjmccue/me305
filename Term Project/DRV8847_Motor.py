'''
@file           DRV8847_Motor.py
@brief          Motor driver class
@details        Implements two classes for driver and motors contains
                fault pin and motor speed control
@author         Tyler McCue, Nick De Simone
@date           October 21st, 2021
'''

import pyb
from pyb import Pin
from pyb import ExtInt
import time

class DRV8847:
    '''
    @brief      Driver class for motor chip
    @details    Motor driver class for pin set up and fault trigger
    '''

    def __init__(self):
        '''
        @brief          Constructor for driver class
        '''

        # Initialize Motor 1 pins to be handled as PWM objects
        self.pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
        self.pinB5 = pyb.Pin(pyb.Pin.cpu.B5)

        # Initialize Motor 2 pins to be handled as PWM objects
        self.pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
        self.pinB1 = pyb.Pin(pyb.Pin.cpu.B1)

        # Initialize the board's nSLEEP pin to be enabled and disabled
        self.pinA15 = pyb.Pin(pyb.Pin.cpu.A15, Pin.OUT_PP)

        # Initialize board's nFAULT pin to disable motor when motor faults
        self.pinB2 = pyb.Pin(pyb.Pin.cpu.B2)
        self.faultInt = pyb.ExtInt(self.pinB2, mode=pyb.ExtInt.IRQ_FALLING,
                                   pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)

        # Define motor timer frequency. Must be >20kHz to avoid noise
        self.tim3 = pyb.Timer(3, freq = 20000)


    def enable(self):
        '''
        @brief         Enables motor driver
        '''
        self.faultInt.disable()
        self.pinA15.value(True)
        time.sleep(.025)
        self.faultInt.enable()

    def disable(self):
        '''
        @brief          Disables motor driver
        '''
        self.pinA15.value(False)

    def fault_cb(self, IRQ_src):
        '''
        @brief          Disables driver after fault trigger
        '''
        print('Motor fault detected')
        print('Disabling motor')
        self.disable()

    def motor(self, motorNum=1):
        '''
        @brief          Creates one of two motor from motor driver class
        @param motorNum Sets which motor will be set up
        '''
        if motorNum == 1:
            t3ch1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinB4)
            t3ch2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinB5)
            return Motor(t3ch1, t3ch2, self)
        else:
            t3ch3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinB0)
            t3ch4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinB1)
            return Motor(t3ch3, t3ch4, self)

class Motor:
    '''
    @brief      Motor class
    @details    Motor class that controls speed and also fault
    '''

    def __init__(self, ch1, ch2, driver):
        '''
        @brief          Constructor for driver class
        @param ch1      Sets first motor channel
        @param ch1      Sets second motor channel
        @param driver   Drive parent class
        '''
        ## creates class variable for channel 1
        self.channel_1 = ch1
        ## creates class variable for channel 2
        self.channel_2 = ch2
        ## creates class variable for parent class
        self.driver = driver


    def set_duty(self, duty):
        '''
        @brief              Changes duty cycle to change speed
        @param duty         Pulse width percent from -100 to 100
        '''
        if duty >= 0:
            self.channel_1.pulse_width_percent(100)
            self.channel_2.pulse_width_percent(100-duty)
        else:
            self.channel_1.pulse_width_percent(100+duty)
            self.channel_2.pulse_width_percent(100)

    def clear_fault(self):
        '''
        @brief              Clears any faults
        '''
        self.driver.enable()
