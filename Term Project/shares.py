'''
@file           shares.py
@brief          Shares class to share data between tasks and classes
@author         Tyler McCue, Nick De Simone
@date           Oct 9, 2021
'''
class Shares:

    def __init__(self, m1, m2, IMU, TS, Control):
        ''' @brief    Contstuctor for shares class
        '''
        self.DataX = [0,0,0,0]
        self.DataY = [0,0,0,0]
        self.m1 = m1
        self.m2 = m2
        self.IMU = IMU
        self.TS = TS
        self.active = False
        self.touch = False
        self.collect = False
        self.Control = Control
