'''
@file           UI_Encoder_Task.property.py
@brief          Driver that takes user input
@details        Uses micro python VCP module to accept user inputs
                one character at a time
@author         Tyler McCue, Nick De Simone
@date           Oct 9, 2021
'''

import pyb

class UI(Encoder):
    '''
    @brief      UI class that allows encoder to take user inputs
    @details    Uses micro python VCP module to accept user inputs
                for encoder

    '''

    def __init__(self):
        '''
        @brief  construct for UI driver
        '''
        ##Assign VP communication for specified encoder
        self.coms = pyb.USB_VCP()
        self.input = '0'
        self.collect_data = False
        self.execute_input = False

    def getInput(self):
        if self.coms.any():
            self.input = self.coms.read(1)
            self.execute_input = True

    def which_task(self):
        if self.execute_input:
            if self.input == 90:
                self.set_position(0)
            if self.input == 50:
                print(self.get_position)
            if self.input == 44:
                print(self.get_delta)
