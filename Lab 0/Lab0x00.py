''' @file Lab0x00.py
Prompts user for a given index and calculates the respective fibonacci
number for that input.
'''

def fib(i):                                                                         #fibonacci number
    fibo = [0,1]                                                                    #initial values
    j = 2
    if j <= i:
        for j in range(i):                                                          #iteration of fibonacci sequence
            idx = j + 2
            fibo += [fibo[idx - 1] + fibo[idx - 2]]
            j += 1
    return fibo[i]                                                                  #gives requested index

if __name__ =='__main__':

    t = True                                                                        #while condition for input loop
    while t:
        s = True
        try:                                                                        #checks if its an integer
            ans = int(input("Please enter wanted fibonacci index: "))
            if ans >= 0:                                                            #checks for non-negative
                print(('Fibonacci number at index {:} is {:}.'.format(ans,fib(ans))))
                while s:
                    ans1 = input("Would you like to calculate another? (y/n): ")    #asks to go again
                    if ans1 == "n" or ans1 == "y":                                  #checks for appropriate response
                        s = False
                        if ans1 == "n":
                            t = False
                    else:
                        print("Please enter either 'y' for yes or 'n' for no")
            else:
                print("Please enter a non-negative number")
        except:
            print("Please enter a number")
