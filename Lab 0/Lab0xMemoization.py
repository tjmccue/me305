def fib(i, memory = {0: 0, 1: 1}):        #Recursive fibonacci function
    if i not in memory:
        memory[i] = fib(i - 1, memory) + fib(i - 2, memory);
    return memory[i] #recursivley calls function

# def memoize(n):
#     memory = {}
#     def remember(x):
#         if x not in memory:
#             memory[x] = n(x)
#         return memory[x]
#     return remember
# fib = memoize(fib)

print(fib(999))
