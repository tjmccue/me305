'''
@file           UI_Encoder_Task.py
@brief          User Interface on Putty Terminal
@details        Uses micro python VCP module to accept user inputs
                one character at a time. These user inputs command action
                by either the motors or encoders.
@author         Tyler McCue, Nick De Simone, Charlie Refvem (Reference)
@date           Oct 9th, 2021
'''

import pyb
import utime
import array as arr


class UI():
    '''
    @brief      UI class that allows encoder to take user inputs
    @details    Uses micro python VCP module to accept user inputs
                for encoder
    '''

    def __init__(self, period):
        '''
        @brief          Constructor for UI driver
        @param period   Period of time at which user task runs, in sec
        '''
        ## Set input period equal to the frequency at which the user task will
        # run
        self.user_freq = period
        # Convert user task period from sec to microsec
        self.user_freq = int(self.user_freq*1E6)

        ## Assign VP communication for specified encoder
        # Allows for reading and writing of data over the USB connecting user
        #   & Nucleo
        self.coms = pyb.USB_VCP()

        ## Initiate input variable associated with user key presses
        self.input = '0'

        ## Initiate state variable sent to encoder task
        self.state = 0

        ## Initiate gather flag, which enables the 30 sec data collection code
        self.gather1 = False
        self.gather2 = False

        ## Next_Time determines the time at which the user task will look for
        #  user inputs
        self.next_time = utime.ticks_add(utime.ticks_us(), self.user_freq)

        ## Initiate data list, which is filled with 30 sec of position and
        #  delta values when a "g" is pressed
        self.data1 = []
        self.data2 = []

        ## Initiate run variable that allows for 30 seconds of data collection
        self.run1 = -self.user_freq/1E6
        self.run2 = -self.user_freq/1E6

        self.closedG1 = False
        self.closedG2 = False

        self.runcl1 = -self.user_freq/1E6
        self.runcl2 = -self.user_freq/1E6

        self.datacl1 = []
        self.datacl2 = []

        # Welcome instructions printed to Putty terminal on start-up
        print('+-----------------------------------------------------------+')
        print('|                  Welcome to EncoderUI                     |')
        print('|                                                           |')
        print('| Encoder Commands:                                         |')
        print('| 1. "z": Reset Encoder 1 position to 0                     |')
        print('| 2. "p": Get current Encoder 1 position                    |')
        print('| 3. "d": Get current Encoder 1 delta                       |')
        print('| 4. "g": Collect Encoder 1 data for 30 seconds             |')
        print('| 5. "s": End Encoder 1 data collection prematurely         |')
        print('|    (Data will show on screen at the end of collection)    |')
        print('| NOTE: For Encoder 2, press the capitalized version of the |')
        print('| above commands                                            |')
        print('|                                                           |')
        print('| Motor Commands:                                           |')
        print('| 1. "m": Prompt user to enter a duty cycle for Motor 1     |')
        print('| 2. "M": Prompt user to enter a duty cycle for Motor 2     |')
        print('| 3. "c" OR "C" : Clear fault conditions                    |')
        print('| NOTE: Duty cycle values must be from -100 to +100         |')
        print('|                                                           |')
        print('+-----------------------------------------------------------+')

    def getInput(self, s):
        '''
        @brief              Reads user inputs and writes encoder/motor commands
        @param s            Refers to shares class which shares needed data
        '''
        if (utime.ticks_diff(utime.ticks_us(), self.next_time) > 0):
            self.next_time = utime.ticks_add(self.next_time, self.user_freq)
            self.state = 0
            # Read one character of user key press
            if self.coms.any():
                self.input = self.coms.read(1)

                # Encoder/Motor 1 key presses and associated commands
                if self.input.decode() == 'z':
                    print('Setting Position of motor 1 to 0')
                    self.state = 1
                if self.input.decode() == 'p':
                    print('Getting Position of motor 1: ', s.data1[0])
                if self.input.decode() == 'd':
                    print('Getting Delta of motor 1: ', s.data1[1]*(2*3.141592/4000)/(s.e1.encoder_freq/1E6))
                if self.input.decode() == 'g':
                    print('Collecting data for motor 1 for 30 seconds')
                    self.gather1 = True
                if self.input.decode() == 's':
                    print('Ending data collection')
                    self.gather1 = False
                if self.input.decode() == 'm':
                    print('Please enter a duty cycle between -100 to 100')
                    input_duty = int(input('Duty Cycle: '))
                    s.m1.set_duty(input_duty)
                if self.input.decode() == '1':
                    self.closedG1 = True
                    print('Please enter a rotational velocity between -200 to 200 rad/s')
                    speed = int(input('Velocity: '))
                    s.cl1.setReference(speed)
                if self.input.decode() == 'c':
                    print('Clearing motor fault')
                    s.m1.clear_fault()

                # Encoder/Motor 2 key presses and associated commands
                if self.input.decode() == 'Z':
                    print('Setting Position of motor 2 to 0')
                    self.state2 = 1
                if self.input.decode() == 'P':
                    print('Getting Position of motor 2: ', s.data2[0])
                if self.input.decode() == 'D':
                    print('Getting Delta of motor 2: ', s.data2[1]*(2*3.141592/4000)*1/(s.e2.encoder_freq/1E6))
                if self.input.decode() == 'M':
                    print('Please enter a duty cycle between -100 to 100')
                    input_duty = int(input('Duty Cycle: '))
                    s.m2.set_duty(input_duty)
                if self.input.decode() == 'G':
                    print('Collecting data for motor 2 for 30 seconds')
                    self.gather2 = True
                if self.input.decode() == 'S':
                    print('Ending data collection')
                    self.gather2 = False
                if self.input.decode() == '2':
                    self.closedG2 = True
                    print('Please enter a rotational velocity between -200 to 200 rad/s')
                    speed = int(input('Velocity: '))
                    s.cl2.setReference(speed)
                if self.input.decode() == 'C':
                    print('Clearing motor fault')
                    s.m2.clear_fault()


                # Evaluating for gather = True allows for the "s" exception, which
                # will end data collection prematurely
            if self.gather1 and (self.run1 < 30.0/(self.user_freq/1E6)):
                self.data1.append(arr.array('f', [s.data1[0], s.data1[1]*(2*3.141592/4000)*1/(s.e1.encoder_freq/1E6), (s.data1[2])/1E6]))
                self.run1 += 1

            else:
                # Print the zeroeth and first index of each list in the data
                #   list, one line at a time
                for i in range(len(self.data1)):
                    print(self.data1[i][0],',', self.data1[i][1],',', self.data1[i][2])
                self.gather1 = False
                self.data1 = []
                self.run1 = -self.user_freq/1E6

            if self.gather2 and (self.run2 < 30.0/(self.user_freq/1E6)):
                self.data2.append(arr.array('f', [s.data2[0], s.data2[1]*(2*3.141592/4000)*1/(s.e2.encoder_freq/1E6), (s.data2[2])/1E6]))
                self.run2 += 1

            else:
                # Print the zeroeth and first index of each list in the data
                #   list, one line at a time
                for i in range(len(self.data2)):
                    print(self.data2[i][0],',', self.data2[i][1],',', self.data2[i][2])
                self.gather2 = False
                self.data2 = []
                self.run2 = -self.user_freq/1E6



            if self.closedG1 and (self.runcl1 < 10.0/(self.user_freq/1E6)):
                self.datacl1.append(arr.array('f', [s.data1[0], s.data1[1]*(2*3.141592/4000)*1/(s.e1.encoder_freq/1E6), (s.data1[2])/1E6]))
                s.cl1.run(s.data1[1]*(2*3.141592/4000)*1/(s.e1.encoder_freq/1E6))
                s.m1.set_duty(s.cl1.actuation)
                self.runcl1 += 1

            else:
                # Print the zeroeth and first index of each list in the data
                #   list, one line at a time
                for i in range(len(self.datacl1)):
                    print(self.datacl1[i][0],',', self.datacl1[i][1],',', self.datacl1[i][2])
                self.closedG1 = False
                self.datacl1 = []
                self.runcl1 = -self.user_freq/1E6

            if self.closedG2 and (self.runcl2 < 10.0/(self.user_freq/1E6)):
                self.datacl2.append(arr.array('f', [s.data2[0], s.data2[1]*(2*3.141592/4000)*1/(s.e2.encoder_freq/1E6), (s.data2[2])/1E6]))
                s.cl2.run(s.data2[1]*(2*3.141592/4000)*1/(s.e2.encoder_freq/1E6))
                s.m2.set_duty(s.cl2.actuation)
                self.runcl2 += 1

            else:
                # Print the zeroeth and first index of each list in the data
                #   list, one line at a time
                for i in range(len(self.datacl2)):
                    print(self.datacl2[i][0],',', self.datacl2[i][1],',', self.datacl2[i][2])
                self.closedG2 = False
                self.datacl2 = []
                self.runcl2 = -self.user_freq/1E6
