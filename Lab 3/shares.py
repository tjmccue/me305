'''
@file           shares.py
@brief          Shares class to share data between tasks and classes
@author         Tyler McCue, Nick De Simone
@date           Oct 9, 2021
'''
class Shares:

    def __init__(self, m1, m2, e1, e2, cl1, cl2):
        self.data1 = [0,0,0]
        self.data2 = [0,0,0]
        self.m1 = m1
        self.m2 = m2
        self.e1 = e1
        self.e2 = e2
        self.cl1 = cl1
        self.cl2 = cl2
        self.state = [0,0]
