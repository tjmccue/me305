'''
@file           main.py
@brief          Runs UI, motor, and encoder tasks
@details        Runs UI, motor, and encoder tasks using
                UI_Encoder_Task, task_motor,
                and task_encoder with PYB module
@author         Tyler McCue, Nick De Simone
@date           Oct 9, 2021
'''

import task_encoder as te
import UI_Encoder_Task as user
import DRV8847_Motor as Dr
import Closed_Loop as cl
import shares as s


if __name__ == '__main__':

    ## Create Motor Driver object
    driver = Dr.DRV8847()

    ## Create Motor objects
    # Specify motor number
    motor1 = driver.motor(1)
    motor2 = driver.motor(2)

    ## Create encoder objects
    # Specify encoder number and task period in [sec]
    encoder_task1 = te.Task_Encoder(1, .2)
    encoder_task2 = te.Task_Encoder(2, .2)

    closed1 = cl.ClosedLoop(-100, 100, .025)
    closed2 = cl.ClosedLoop(-100, 100, .025)

    share =s.Shares(motor1, motor2, encoder_task1, encoder_task2, closed1, closed2)

    ## Create user interface object
    # Specify user task period in [sec]
    user_input = user.UI(0.25)

    # Turn motors on using DRV8847 method
    driver.enable()

    while (True):

        # Attempt to run FSM unless Ctrl+C is hit
        try:
            user_input.getInput(share)
            encoder_task1.get_update(share)
            encoder_task2.get_update(share)

        #Look for Ctrl+C which triggers a KeyboardInterrupt
        except KeyboardInterrupt:
            break # Breaks out of the FSM while loop

    # Actions taken once while loop is terminated
    # Turn motors off using DRV8847 method
    driver.disable()
    print('Program Terminating')
