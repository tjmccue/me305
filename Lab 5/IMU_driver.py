# -*- coding: utf-8 -*-
''' @file                       IMU_Driver.py
    @brief                      A driver for reading IMU values
    @author                     Tyler McCue, Nick De Simone
    @date                       November 5th, 2021
'''
from pyb import I2C

class IMU_Driver:
    ''' @brief        IMU driver to set up pins and retrieve values
        @details      Driver for IMU to do a range of things and measure
                      orientation and speed data
    '''

    def __init__(self, id=0x28):
        ''' @brief      Constructor for IMU
            @details    Sets up IMU to read and write to memory
            @param  id  ID address for IMU
        '''
        ## Creates class variable for I2C
        self.i2c = I2C(1)
        ## Creates IMU as master
        self.i2c = I2C(1, I2C.MASTER)
        ## Creates bytearray as buffer
        self.buff = bytearray()
        ## ID for IMU
        self.id = id

        ## mode = dictionary of mode values
        # Mode Value: Decimal Value
        # return decimal value after specifying mode value
        self.mode = {"ACCONLY": 1,
        "MAGONLY": 2,
        "GYROONLY": 3,
        "ACCMAG": 4,
        "ACCGYRO": 5,
        "MAGGYRO": 6,
        "AMG": 7,
        "IMU": 8,
        "COMPASS": 9,
        "NDOF_FMC_OFF": 11,
        "NDOF": 12}

    def change_mode(self, mode_value):
        ''' @brief              Changes IMU mode
            @details            Changes IMU mode based on sensors available
            @param  mode_value  mode name
        '''
        self.i2c.mem_write(self.mode.get(mode_value), self.id, 0x3D)

    def get_calibration_status(self):
        ''' @brief              Gets calibration status of IMU
            @details            Gets 3 numbers (0:3) from IMU calibration 3 being ready
        '''
        self.i2c.mem_read(8,40,35)

    def get_calibration_coeffs(self):
        ''' @brief              Gets calibration coefficients
            @details            Gets coefficients for each sensor for fast calibration
        '''
        buff = bytearray(22)
        return self.i2c.mem_read(buff,40,55)

    def write_calibration_coeffs(self, b_buffer):
        ''' @brief              Write calibration coefficients
        '''
        self.i2c.mem_write(b_buffer,40,55)

    def get_Euler_angles(self):
        ''' @details              Reads IMU data as Euler angles and gives
                                  heading, pitch and roll
            @return               Returns tuple of head, pitch, and roll
        '''
        head = bytearray(self.i2c.mem_read(2,40,0x1A))
        head_value = int.from_bytes(head, "little", False)
        roll = bytearray(self.i2c.mem_read(2,40,0x1C))
        roll_value = int.from_bytes(roll, "little", False)
        pitch = bytearray(self.i2c.mem_read(2,40,0x1E))
        pitch_value = int.from_bytes(pitch, "little", False)
        if head_value > 32767:
            head_value -= 65536
        if roll_value > 32767:
            roll_value -= 65536
        if pitch_value > 32767:
            pitch_value -= 65536
        return head_value/16, pitch_value/16, roll_value/16
